<?php 
/*
Plugin Name: Redirection automatique, par Wabeo
Plugin URI: https://bitbucket.org/willybahuaud/redirection-automatique-wp/
Description: Redirection 301 automatique des contenus supprimés, vers leur page d'archive
Version: 0.9
Author URI: http://wabeo.fr/
*/

if ( !function_exists( 'add_action' ) ) {
	echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
	exit;
}

add_action( 'before_delete_post', 'willy_get_old_uris' );
function willy_get_old_uris( $postid ) {
	if ( ! wp_is_post_revision( $postid ) ) {
		$old = get_option( 'old_uris' );
		if ( ! $old ) {
			$old = array();
		}
		$new = get_permalink( $postid );
		$post_type = get_post_type( $postid );
		$old[ $post_type ][] = $new;
		update_option( 'old_uris', $old );
	}
}

add_action('template_redirect', 'willy_404_redirect');
function willy_404_redirect() {
    if( is_404() ){
    	global $wp;
		$current_url = trailingslashit( home_url( $wp->request ) );
		$old = get_option( 'old_uris' );
		if ( $old ) {
			foreach ( $old as $k => $cpt ) {
				if ( in_array( $current_url, $cpt ) ) {
			    	$link = get_post_type_archive_link( $k );
			    	if ( ! $link ) {
			    		if ( 'post' == $k && ( $post_archive = get_option( 'page_for_posts' ) ) ) {
			    			$link = get_permalink( $post_archive );
			    		} else {
			    			$link = home_url();
			    		}
			    	}
			        wp_redirect( $link, 301 );
			        exit;
			    }
		    }
        }
    }
}

add_action( 'admin_menu', 'willy_menu_old_uris' );
function willy_menu_old_uris() {
	add_submenu_page( 'options-general.php', 'Gestions des redirections automatiques', 'Redirections auto', 'manage_options', 'redirect-auto', 'willy_menu_404' );
}

function willy_menu_404() {
	echo '<div class="wrap"><div id="icon-tools" class="icon32"></div>';
		echo '<h2>Redirections automatiques</h2>';
		echo '<form method="POST" action="options.php">';
		settings_fields( 'redirect-auto' );
		do_settings_sections( 'redirect-auto' );
		submit_button();
		echo '</form>';
	echo '</div>';
}

function eg_settings_api_init() {
 	add_settings_section(
		'redirect_auto_section',
		'Liste des urls supprimées, à rediriger',
		false,
		'redirect-auto'
	);

 	add_settings_field(
		'old_uris',
		'Anciennes urls',
		'willy_liste_urls',
		'redirect-auto',
		'redirect_auto_section'
	);

	function willy_liste_urls() {
		$old_uris = get_option( 'old_uris' );
		foreach( $old_uris as $k => $cpt ){
			$obj = get_post_type_object( $k );
			if ( $obj ) {
				echo '<p><label for="old_uris[' . $k . ']">' . $obj->labels->name . '</label>';
				echo '<textarea style="width:100%;height:200px;" name="old_uris[' . $k . ']">';
				if( is_array( $cpt ) ) {
					$cpt = implode( "\r\n", $cpt );
					echo $cpt;
				}
				echo '</textarea></p>';
			}
		}
	}

 	register_setting( 'redirect-auto', 'old_uris', 'willy_save_old_uris' );
}

function willy_save_old_uris( $old_uris ) {
	if ( $old_uris && is_array( $old_uris ) ) {
		foreach ( $old_uris as $k => $cpts ) {
			if ( ! is_array( $cpt ) ) {
				$cpt = explode( "\r\n", $cpt );
				$cpt = array_filter( $cpt );
			}
		}
	}
	return $old_uris;
}
 
 add_action( 'admin_init', 'eg_settings_api_init' );
