# Redirection automatique WP #

Ce plugin WordPress permet de rediriger les URLs des contenus WordPress supprimés vers l'archive correspondante ou la page d'accueil du site le cas échéant. 

### Comment modifier les URLs à rediriger ? ###

Les URLs à rediriger sont administrables via le menu "Réglages > Redirection auto".

### Installation ###

* Uploader ce plugin dans le répertoire plugins de WordPress
* Activer le plugin dans l'administration de WordPress

ou

* Uploader ce plugin dans le répertoire mu-plugins de WordPress
